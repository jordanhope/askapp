QSource App

QSource is an application designed to let users ask questions in a Q&A setting without needing to wait for a microphone.
It also allows for the questions to be moderated as to avoid wasting time and keeping the conversation on topic. I created
this app as part of my senior project as an experiment in Polymer to teach myself how to build an app using Polymer and Firebase.

The application can be found at https://askapp-ddc29.firebaseapp.com/